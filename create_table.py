import boto3


def create_data_table(dynamodb=None):
    if not dynamodb:
        #dynamodb = boto3.resource('dynamodb', endpoint_url="http://localhost:8000")
        dynamodb = boto3.resource('dynamodb', region_name='ca-central-1')

    table = dynamodb.create_table(
        TableName='COVIDData',
        KeySchema=[
            {
                'AttributeName': 'date',
                'KeyType': 'HASH'  # Partition key
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'date',    # you need an attribute definition for each key
                'AttributeType': 'S'    # type = string
            },

        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )
    return table


if __name__ == '__main__':
    data_table = create_data_table()
    print("Table status:", data_table.table_status)