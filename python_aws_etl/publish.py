import boto3

client = boto3.client('sns')

topic = 'arn:aws:sns:ca-central-1:186430262711:pythonCOVIDEvent'

response = client.publish(
    TopicArn=topic,
    Message='This is sent from a python program',
    Subject='Test from python'
)
print(response)