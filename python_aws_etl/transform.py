from datetime import datetime

def combine_sets(x, y):
    all_data = []
    for datex, *restx in x:
        for datey, *resty in y:
            if datex == datey:
                all_data.append([datex, *restx, *resty])
    return all_data

def split_usa(line):
    date_str,cases,deaths = line.decode('utf8').strip().split(',')
    return date_str,cases,deaths

def split_world(line):
    date_str,cases,_recovered,deaths,*_ = line.decode('utf8').strip().split(',')
    return date_str,cases,deaths

parse_options = {
    "usa": split_usa,
    "world": split_world
}

def parse_text_data(data_text, type):
    data_list = []
    next(data_text)
    for line in data_text:
        date_str,cases,deaths = parse_options[type](line)
        #print(date)
        date_obj = datetime.fromisoformat(date_str)     #EST
        data_list.append([date_obj,cases,deaths])
    data_text.close()
    return data_list


def parse_data(usa_text, world_text):
    usa_list = parse_text_data(usa_text, "usa")
    world_list = parse_text_data(world_text, "world")
    all_data = combine_sets(usa_list, world_list)
    return all_data