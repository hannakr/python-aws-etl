#!/bin/bash
zip program python_aws_etl/get.py python_aws_etl/transform.py
aws lambda update-function-code --function-name eventDrivenPython --zip-file fileb://program.zip
