import boto3

def delete_data_table(dynamodb=None):
    if not dynamodb:
        dynamodb = boto3.resource('dynamodb', endpoint_url="http://localhost:8000")

    table = dynamodb.Table('COVIDData')
    table.delete()


if __name__ == '__main__':
    delete_data_table()
    print("COVIDData table deleted.")